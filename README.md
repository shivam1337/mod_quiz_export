Quiz export plugin for moodle.

Type: Block plugin

Author: Shivam P. Marmat
email:  marmat.shivam99@gmail.com

If phpWord is not installed in your system install it using composer.
Installation steps are as follows:
1. Download and install Composer
    https://getcomposer.org/download/
2. Go to project root directory
3. Type following command in prompt
    $composer require phpoffice/phpword
4. In the php file add
    require_once 'vendor/autoload.php';
    (if vendor folder not in root directory find it 
     and add appropriate link in above code)