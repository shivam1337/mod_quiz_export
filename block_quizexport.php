<?php
class block_quizexport extends block_base {
    public function init() {
        $this->title = get_string('quizexport', 'block_quizexport');
    }
	public function applicable_formats() {
		return array ('course-view' => true);
	}
	public function get_content_type() {
		return BLOCK_TYPE_TEXT;
	}
    // The PHP tag and the curly bracket for the class definition 
    // will only be closed after there is another function added in the next section.
	public function get_content() {
		if ($this->content !== null) {
		  return $this->content;
		}
	
		$this->content         =  new stdClass;
		$this->content->text   = 'The content of our quiz export block!';
		//$this->content->footer = 'Footer here...';
		
		//$this->content->text .= json_encode(parent::applicable_formats());
		//return $this->content;
		global $COURSE, $DB;
		$url = new moodle_url('/blocks/quizexport/export.php', array('blockid' => $this->instance->id, 'courseid' => $COURSE->id));
		//$this->content->footer = html_writer::link($url, get_string('exportquiz', 'block_quizexport'));
		/*if (!empty($this->config->text)) {
			$this->content->text = $this->config->text;
		} 
		*/ 
		// This is the new code.
		
		if ($quizzes = $DB->get_records('quiz')) {
			$this->content->text .= html_writer::start_tag('ul');
		foreach ($quizzes as $quiz) {
				$pageurl = new moodle_url('/blocks/quizexport/export.php',
					array('blockid' => $this->instance->id, 'courseid' => $COURSE->id,
						'id' => $quiz->id, 'viewpage' => '1', 'name'=>$quiz->name));
				$this->content->text .= html_writer::start_tag('li');
				$this->content->text .= html_writer::link($pageurl, $quiz->name);
				$this->content->text .= html_writer::end_tag('li');
			}
			$this->content->text .= html_writer::end_tag('ul');
		}
		
		return $this->content;
	}
	public function hide_header() {
		return false;
	}
	public function instance_delete() {
		global $DB;
		$DB->delete_records('block_quizexport', array('blockid' => $this->instance->id));
	}
	/*(public function _self_test() {
		return true;
	} */
}
