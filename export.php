<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

 /**
  *Script to Export quiz as a word file.
  *
  *@package		moodle_mod
  *@subpackage	quiz
  *@copyright 	2019 onwards Shivam Poonamchand Marmat {@link marmat.shivam99@gmail.com}
  *@license		http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later	
  */  


require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/blocks/quizexport/editlib.php');
//loading files for phpWord plugin
require_once($CFG->dirroot . '/vendor/autoload.php');

$quiz_id = required_param('id', PARAM_INT);
$quiz_name = required_param('name', PARAM_ALPHA);
$courseid = required_param('courseid', PARAM_INT);

list($thispageurl, $contexts, $cmid, $cm, $module, $pagevars) =
        question_edit_setup('export', '/blocks/quizexport/export.php');

// get display strings
$strexportquestions = get_string('exportquestions', 'question');

list($catid, $catcontext) = explode(',', $pagevars['cat']);

//php word object
$phpWord = new \PhpOffice\PhpWord\PhpWord();
use PhpOffice\PhpWord\IOFactory;
use \PhpOffice\PhpWord\Settings;
	
//creating new section	
$section = $phpWord->addSection();

$section->addText('COLLEGE OF ENGINEERING, PUNE', array('bold' => true, 'size' => 16, 'align' => 'center'));
$section->addText('(An Autonomous Institute of Govt. of Maharashtra)', array('bold' => true, 'size' => 11));
$section->addText('End Semester Examination – 19 Nov 2015', array('bold' => true, 'size' => 12, 'align' => 'center'));
$section->addText('Course Name', array('bold' => true, 'size' => 12, 'align' => 'center'));
$section->addText('Class: - F.Y./S.Y./T.Y. B.Tech (Computer Engineering and Information Technology)', array('bold' => true, 'size' => 11, 'align' => 'center'));
$section->addText('Year: - 2015-16	Semester: - III', array('bold' => true, 'size' => 10, 'align' => 'center'));
$section->addText('Duration: - X hours	Max. Marks: -  XX' , array('bold' => true, 'size' => 10, 'align' => 'center'));
$section->addText('Instructions:', array('bold' => true, 'size' => 10, 'align' => 'center'));
$section->addText('1.	All questions are compulsory.');
$section->addText('2.	This is an open book test. You can read any number of textbooks and one notebook. Photocopies  or prints of programs are not permitted.');
$section->addText('3.	Whenever provided, use the given type definitions and function prototypes for writing your answers.');
$section->addText('4.	State all assumptions that you make.');
$section->addText('5.	Code must be indented, commented, and written in ANSI C. You can write the code with a bold pencil.');
$section->addText('6.	You can explain your answer in Marathi/Hindi.');

// Header
	$PAGE->set_url($thispageurl);
	$PAGE->set_title($strexportquestions);
	$PAGE->set_heading($COURSE->fullname);
	echo $OUTPUT->header();
	
	/* Following code access the mdl_quiz_slots database table
	 * and gets the slots for quiz_id obtained from url
	 */
	$quiz_slots = $DB->get_records('quiz_slots', array('quizid' => $quiz_id));

	/* Following code access mdl_question database table 
	 * and select values id, name,  questiontext, qtype, questiontextformat
	 * for condition slot->questionid
	 * i.e. only questions from question bank selected available in slot
	 */
	$questions[] = array();
	foreach($quiz_slots as $slot){
		$questions[] = $DB->get_record_sql('SELECT id, name, questiontext, qtype, questiontextformat FROM mdl_question WHERE id = ?', array($slot->questionid));
	}
	/* Following code acess the mdl_question_answers database 
	 * table on the condition for condition question->id
	 * for the questions in $questions array.
	 */

	$answers[] = array();
	foreach($questions as $question){
		if($question->qtype == 'multichoice'){
			$answers[] = $DB->get_records_sql('SELECT answer FROM mdl_question_answers WHERE question = ?', array($question->id));
		}else if($question->qtype == 'truefalse'){
			$answers[] = $DB->get_records_sql('SELECT answer FROM mdl_question_answers WHERE question = ?', array($question->id));
		}else if($question->qtype == 'shortanswer'){
			$answers[] = $DB->get_record_sql('SELECT answer FROM mdl_question_answers WHERE question = ?', array($question->id));
		}else if($question->qtype == 'numerical'){
			$answers[] = $DB->get_record_sql('SELECT answer FROM mdl_question_answers WHERE question = ?', array($question->id));
		}else{
			var_dump("invalid_question_type!");
		}
	}
	
	//variable initialization
	$i = $j = $k = $l = 1;
	//options array for mcq and truefalse type questions
	$options = array("(a)", "(b)", "(c)", "(d)", "(e)", "(f)", "(g)", "(h)", "(i)", "(j)", "(k)", "(l)");
	//add quiz_name at the begining of quiz
	//$section->addText($quiz_name);
	
	/* Following code add question number, 
	 * question text and options for mcq
	 * and truefalse type questions
	 * Then adds the 'Ans:' for writing answers.
	 */
	 
	$tableStyle = array(
		'borderColor' => '006699',
		'borderSize'  => 2,
		'cellMargin'  => 50
	);
	$firstRowStyle = array('bgColor' => '000');
	$phpWord->addTableStyle('quiz', $tableStyle, $firstRowStyle);
	$table = $section->addTable('quiz');
	
	$styleCell = array('valign'=>'center');
	
	foreach($questions as $question){
		if($question->qtype == 'multichoice'){
			$table->addRow(900);
			$table->addCell(2000, $styleCell)->addText($k);
			$table->addCell(2000, $styleCell)->addText(strip_tags($question->questiontext));
			//$table->addCell(2000, $styleCell)->addText('');
			foreach($quiz_slots as $slot){
				if($slot->questionid == $question->id){
					$table->addCell(2000, $styleCell)->addText(sprintf("%.2f",$slot->maxmark));
					break;
				}
			}
			$k++;
			$j = 0;
			$table->addRow(900);
			$table->addCell(2000, $styleCell)->addText('');
			$temp = $table->addCell(2000, $styleCell);
			foreach($answers[$i] as $answer){
				$temp->addText('    '.$options[$j].' '.strip_tags($answer->answer));
				$j++;
			}
			$table->addCell(2000, $styleCell)->addText('');
			$i++;
		}else if($question->qtype == 'truefalse'){
			$table->addRow(900);
			$table->addCell(2000, $styleCell)->addText($k);
			$table->addCell(2000, $styleCell)->addText(strip_tags($question->questiontext));
			//$table->addCell(2000, $styleCell)->addText('');
			foreach($quiz_slots as $slot){
				if($slot->questionid == $question->id){
					$table->addCell(2000, $styleCell)->addText(sprintf("%.2f",$slot->maxmark));
					break;
				}
			}
			$k++;
			$j = 0;
			$table->addRow(900);
			$table->addCell(2000, $styleCell)->addText('');
			$temp = $table->addCell(2000, $styleCell);
			foreach($answers[$i] as $answer){
				$temp->addText('    '.$options[$j].' '.strip_tags($answer->answer));
				$j++;
			}
			$table->addCell(2000, $styleCell)->addText('');
			$i++;			
		}else if($question->qtype == 'shortanswer'){
			$table->addRow(900);
			$table->addCell(2000, $styleCell)->addText($k);
			$table->addCell(2000, $styleCell)->addText(strip_tags($question->questiontext));
			//$table->addCell(2000, $styleCell)->addText('');
			foreach($quiz_slots as $slot){
				if($slot->questionid == $question->id){
					$table->addCell(2000, $styleCell)->addText(sprintf("%.2f",$slot->maxmark));
					break;
				}
			}
			$k++;
			$i++;
		}else if($question->qtype == 'numerical'){
			$table->addRow(900);
			$table->addCell(2000, $styleCell)->addText($k);
			$table->addCell(2000, $styleCell)->addText(strip_tags($question->questiontext));
			//$table->addCell(2000, $styleCell)->addText('');
			foreach($quiz_slots as $slot){
				if($slot->questionid == $question->id){
					$table->addCell(2000, $styleCell)->addText(sprintf("%.2f",$slot->maxmark));
					break;
				}
			}
			$k++;
			$i++;
		}
	}
	
	// Saving the document as OOXML file...
	$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
	$objWriter->save($quiz_name.'.docx');
	
	// Saving the document as PDF file...
	// Make sure you have `dompdf/dompdf` in your composer dependencies.
	Settings::setPdfRendererName(Settings::PDF_RENDERER_DOMPDF);
	// Any writable directory here. It will be ignored.
	Settings::setPdfRendererPath('.');

	$phpWord = IOFactory::load($quiz_name.'.docx', 'Word2007');
	$phpWord->save($quiz_name.'.pdf', 'PDF');
	
	echo $OUTPUT->continue_button(new moodle_url('answer.php?id='.$quiz_id.'&name='.$quiz_name.'&courseid='.$courseid));
	
?>