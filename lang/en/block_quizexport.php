<?php
$string['pluginname'] = 'Quiz Export';
$string['quizexport'] = 'Quiz Export';
$string['quizexport:addinstance'] = 'Add a new quiz export block';
$string['quizexport:myaddinstance'] = 'Add a new quiz export block to the My Moodle page';
$string['quizexportsettings'] = 'Settings for quiz export block';
$string['editpage'] = 'Edit quiz export block';
$string['addpage'] = 'Add page';
$string['exportquiz'] = 'Quiz';
?>